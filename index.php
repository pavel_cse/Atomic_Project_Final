<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Atomic Project</title>
        <!-- Bootstrap -->
        <link href="Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="Resource/css/style1.css" rel="stylesheet">
        
    </head>
    <body>
	<div id="aa" class="container" align="center" id="sub_by">
	 <h2>BITM - Web App Development - PHP</h2>
		<div class="me">
	     
            <h3>Submitted By</h3>
            <p>Name: Pavel Parvej</p>
            <p>SEIP ID : 106611</p>
            <p>Batch : 11</p>
        </div>
		</div>
		
		
		
       <div align="center" class="container">
       
     
        <br>
        <table id="bb"class="table table-bordered  table-hover text-left ">
            <thead>
                <tr class="success">
                    <th style="text-align: center;color:;font-size: 18px">Sl.</th>
                    <th style="text-align: center;color:;font-size: 18px">Project Name</th>
                </tr>
            </thead>
            <tbody> 
                <tr>
                    <td>01</td>
                    <td><a href="views/SEIP106611/Birthday/index.php">Birthday List</a></td>
		</tr>
					
                <tr>
                    <td>02</td>
                    <td><a href="views/SEIP106611/Book/index.php">Favourite Books List</a></td>
                </tr>
				
		<tr>
                    <td>03</td>
                    <td><a href="#">Terms & Condition</a></td>
		</tr>
				
		<tr>
                    <td>04</td>
                    <td><a href="views/BITM/SEIP106611/Hobby/index.php">Hobby List</a></td>
		</tr>
				
		<tr>
                    <td>05</td>
                    <td><a href="#">City Selection</a></td>
		</tr>
				
		<tr>
                    <td>06</td>
                    <td><a href="views/BITM/SEIP106611/Gender/index.php">Gender Selection</a></td>
		</tr>
				
		<tr>
                    <td>07</td>
                    <td><a href="views/SEIP106611/Email/index.php">Email Subscriptions</a></td>
		</tr>
				
		<tr>
                    <td>08</td>
                    <td><a href="views/SEIP106611/Company/index.php">Summary of Organization</a></td>
		</tr>
				
		<tr>
                    <td>09</td>
                    <td><a href="#">Profile Picture</a></td>
		</tr>
				
            </tbody>
        </table>
        

      
		
	</div>
    </body>
</html>
