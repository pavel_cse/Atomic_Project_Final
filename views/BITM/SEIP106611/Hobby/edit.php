<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\SEIP106611\Hobby\Hobby;
use \App\BITM\SEIP106611\Utility\Utility;
    $hobbyItem = new Hobby();
    $hobby = $hobbyItem->show($_GET["id"]);

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hobby Selection</title>
	<link rel="stylesheet" href="../../../../Resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">
  </head>
  <body>
      <div class="create_wrapper">
		<div><h1 align="center">Change Your Hobby</h1></div>
	  <br />
	  <br />
	  <br />
          <form class="form-horizontal" role="form" action="update.php" method="post">
			<div class="form-group">
				  <label class="control-label col-sm-3" for="field1">Name:</label>
                  <div class="col-sm-3">
                  	<input type="hidden" name="id" value="<?php echo $hobby->id;?>">
                    <input type="text" name="name" class="form-control" id="field1" value="<?php echo $hobby->name;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-3" for="field2">Hobby:</label>
                  <div class="col-sm-9">
                    <div class="checkbox">
                      <label>
						<input type="checkbox" name="hobby" value="Cricket" <?php if(trim($hobby->hobby) == "Cricket"){echo " checked";} ?>>Cricket
					</label> 
					&nbsp;&nbsp;
					<input type="checkbox" name="hobby[]" value="Football" <?php if(trim($hobby->hobby) == "Football"){echo " checked";} ?>>Football
					  </label>
					  &nbsp;&nbsp; <label>
						<input type="checkbox" name="hobby[]" value="Hocky" <?php if(trim($hobby->hobby) == "Hocky"){echo " checked";} ?>>Hocky
					  </label>
					  &nbsp;&nbsp; <label>
						<input type="checkbox" name="hobby[]" value="Pool" <?php if(trim($hobby->hobby) == "Pool"){echo " checked";} ?>>Pool
					  </label>
					  &nbsp;&nbsp; <label>
						<input type="checkbox" name="hobby[]" value="Card" <?php if(trim($hobby->hobby) == "Card"){echo " checked";} ?>>Card
					  </label>
					  &nbsp;&nbsp;
					  <label>
						<input type="checkbox" name="hobby[]" value="Movie" <?php if(trim($hobby->hobby) == "Movie"){echo " checked";} ?>>Movie
					  </label>

                    </div>
                  </div>
                </div>
                <div class="form-group">        
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default" name="submit">Update Hobby</button>
                  </div>
                </div>
              </form>
          <p class="text-center"><a href="../../index.php">Go to Homepage</a> | <a href="index.php">Go to Hobby List</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>