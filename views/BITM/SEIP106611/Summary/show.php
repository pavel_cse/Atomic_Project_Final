<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\SEIP106611\Summary\Summary;


    
    $contantItem = new Summary();
    $contant = $contantItem->show($_GET["id"]);

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Summary List</title>
	<link rel="stylesheet" href="../../../../Resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">
  </head>
  <body>
      <div class="create_wrapper">
          <h1>Your Summary List: </h1>
          <table class="table">
            <tr class="success">
              <td>ID : <?php echo $contant->id; ?></td>
            </tr>
            <tr class="info">
              <td>Name : <?php echo $contant->name; ?></td>
            </tr>
            <tr class="success">
              <td>Summary : <?php echo $contant->contant; ?></td>
            </tr>
          </table>

          <p class="text-center"><a href="index.php">Go to Summary List</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>